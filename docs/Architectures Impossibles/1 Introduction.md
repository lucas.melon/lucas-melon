# 1. Introduction
---
glightbox: false
---
On peut définir une architecture impossible comme une architecture qui s’affranchit d’une des règles fondamentales de la réalité physique, rendant sa construction illusoire. Si pendant longtemps ce type d’architecture a été uniquement spéculatif, le numérique a permis d’aborder les espaces géométriquement différents avec une démarche d’exploration instinctive. Le numérique a offert à toutes ces architectures de fiction un endroit où exister.

Ce mémoire-projet porte sur l’opportunité que représente la simulation d’espaces numériques pour les architectures fictives, particulièrement les architectures impossibles dans la réalité physique. L’objectif est de définir les outils et ressentis architecturaux propres aux espaces numériques grâce à la construction de terrains d’expérimentations numériques pour explorer les principes propres à une architecture impossible.

Au cours de ce mémoire-projet, nous retracerons la pensée non euclidienne depuis ses origines pour tenter d’identifier des principes relatifs à l’architecture impossible, ensuite nous concevrons autour de ces principes huit terrains d’expérimentation en vue de sessions de test en réalité « virtuelle ». Après cela nous dresserons pour chaque terrain un compte rendu des effets et perceptions architecturales provoqués par l’espace. Pour finir, le secteur de recherche étant relativement jeune, de nombreux domaines d’influence ont été évoqués durant la conception, nous aborderons ces domaines successivement à la fin du mémoire. Nous parlerons notamment des enjeux politiques des espaces numériques dans un contexte où ceux-ci prennent de plus en plus de place dans la sphère publique, des dilemmes de représentation d’espaces non euclidiens, des limites logicielles, matérielles, mais aussi physiologiques de la conception numérique. Nous aborderons aussi l’ergonomie propre au numérique et la perméabilité entre la réalité numérique et physique.

## Approche historique

### Origine

Historiquement, l’architecture impossible est indissociable de l’architecture fictive que l’on voit émerger tout d’abord sous forme d’illustration de récits religieux ou mythologiques, le symbolisme divin supplante la réalité.
Parmi ces représentations, certaines sont plus architecturales. On peut notamment citer la tour de Babel de Pieter Brueghel l’Ancien vers 1563. Déjà on peut remarquer une distanciation par rapport aux limites physiques via l’échelle du bâtiment (justifié par son caractère mythique).

![](../images/MEMO/ref1.jpg)
La grande tour de Babel

On peut distinguer des illustrations de bâtiments mythiques, les architectures de papier qui prennent une forme de projet et utilisent les outils de l’architecture (élévation, plan).
L’architecture utopiste pourrait rentrer dans une définition large de l’architecture impossible mais sa fonction spéculative la rattache fondamentalement à la réalité physique. En effet, les utopies tout comme les dystopies existent en contraste avec la réalité dont elles sont des critiques, elles spéculent sur une situation socio-économique différente. Leur impossibilité repose sur les moyens mis en œuvre pour construire un bâtiment, cependant ces bâtiments pourraient émerger dans un autre contexte socio-économique. Les architectures utopistes ne font donc pas exactement partie de notre sujet. 

C’est dans l’illustration que l’on retrouve le plus d’exemples d’architecture impossible.

Giovanni Battista Piranesi explore dans sa série des carceri d’invenzione (1761) une forme de folie géométrique, c’est-à-dire une géométrie dont on ne peut saisir les limites. Grâce à la surcharge d’information et de légères troncatures, il fait perdre ses repères au spectateur, lui faisant ressentir que ce monde ne peut pas être le sien.

![](../images/MEMO/ref2.jpg)
Carceri d’invenzione – planche XI

Probablement l’artiste qui a le plus travaillé sur la géométrie non euclidienne en 2D, Maurits Cornelis Escher (1898-1972) a apporté à son travail une étude mathématique pour représenter au mieux les impossibilités d’espace fictionnel, il utilise des illusions et des trompe-l’œil, des déformations et des abstractions pour permettre une visualisation d’espaces que l’on ne pourra jamais arpenter.

![](../images/MEMO/ref3.jpg)
Relativity

![](../images/MEMO/ref4.jpg)
Belvedere

Gérard Trignac (1955) a développé dans ses gravures des villes fantasmées où l’échelle et la temporalité n’ont plus rien d’humain. Il s’attache plus au ressenti, à l’ambiance que ce type de lieu procure. La logique constructive est proche de la nôtre, mais les matériaux, l’échelle et la programmatique sont incohérents.

![](../images/MEMO/ref5.jpg)
Estampe Strasbourg

On pourrait citer rapidement d’autres auteurs et illustrateurs tels que R.H. Giger ou Tsutomu Nihei pour la bande dessinée ou toute la mouvance de l’architecture surréaliste mais c’est grâce au numérique que l’architecture impossible va vraiment prendre de l’essor.

![](../images/MEMO/ref6.jpg)
Blame! Par Tsutomu Nihei


### Le numérique

Tout d’abord, le numérique a permis de perfectionner la démarche mathématique de M.C. Escher. Dans un de ses cours, le professeur en mathématique appliquée et statistique (à l’université de Nancy Henri-Poincaré) Sébastien Chaumont détermine grâce à des outils numériques le rendu du centre du tableau « Print Gallery ». Pourtant laissé vide par l’auteur, le centre de ce tableau est l’endroit où les grilles de déformation se rejoignent, ce qui engendre un déploiement fractal qui peut difficilement être calculé manuellement.

Le numérique, et avec lui la photographie numérique, permit une approche plus viscérale des espaces impossibles. L’illusion se veut plus subtile et un certain réalisme dans la représentation est recherché. On trouve beaucoup d’exemples dans l’architecture surréaliste, notamment avec des artistes comme Filip Dujardin.

![](../images/MEMO/ref7.jpg)
Montage Par Filip Dujardin

### Le jeux video

Dans les années 90, l’amélioration de capacités techniques permit le passage des jeux vidéo de la 2D à la 3D et avec cela une spatialisation de la discipline. Le principal moyen d’interaction entre le joueur et le développeur est l’espace numérique. Comme pour l’architecture, le rapport entre l’utilisateur et son environnement est au cœur de la réflexion. Les développeurs à la recherche de principes de jeux (gameplay) ont déjà développé une logique architecturale propre aux jeux vidéo et exploitent les différences entre la réalité physique et la simulation de réalité que représente le jeu vidéo pour créer des propositions d’espaces impossibles.

La plupart de ces jeux sont des puzzles Games ou des jeux d’exploration. On trouve Monument Valley dont la perspective change l’environnement et les chemins possibles, Antichamber et ses dimensions imbriquées, les mégastructures brutalistes de Fugue in Void et NaissanceE ou les patterns infinis de Manifold Garden.

![](../images/MEMO/ref8.jpg)
Monument Valley                                                                         

![](../images/MEMO/ref9.jpg)
NaissanceE

![](../images/MEMO/ref10.jpg)
Antichamber

![](../images/MEMO/ref11.jpg)
Fugue In Void

![](../images/MEMO/ref12.jpg)
Manifold Garden

![](../images/MEMO/ref13.jpg)
Fragment of Euclid

![](../images/MEMO/ref14.jpg)
Portal 1


Avec la popularisation des espaces numériques, un vaste public s’est pour la première fois confronté à une architecture de papier non plus avec une démarche de projection spéculative à travers des plans ou rendus, mais avec une démarche d’exploration instinctive.

## Enjeux

Parallèlement à cela de nouveaux enjeux apparaissent autour de la question des espaces numériques. Si le numérique a déjà une place importante dans nos vies, des projets comme le Metavers de l’entreprise Meta (Facebook) ont pour objectif de spatialiser nos interactions numériques, les espaces numériques devenant non plus des espaces exclusivement dédiés à l’art et la science, mais également des espaces de travail ou des espaces publics. Une question se pose donc autour de la qualité de vie qu’offrent les espaces numériques dans le cadre d’un espace de travail et autour de la responsabilité de l'État et des architectes vis-à-vis de cette question.

## Question de recherche

Dans un contexte où les espaces numériques prennent une place de plus en plus importante dans la sphère publique, ce mémoire a pour ambition l’identification des effets architecturaux propres à l’architecture impossible numérique et des outils d’architecture pertinents pour représenter et concevoir ces espaces numériques.
