# 5. Fiche de résultat


## Hall de Connecteurs

### Représentation axionométrique

![](../images/MEMO/fich1.1.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/TV8Zi0jG-nI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Compte rendu de la première session

Perceptions architecturales

Les pièces non caractérisées par une couleur sont difficiles à identifier, ce qui entache l’orientation.
L’espace cyclique du couloir et la présence limitée du soleil ne permettent pas d’identifier facilement un point de référence.
L’espace clôturé par le hall génère un sentiment d’enfermement.
Les pièces connectées au hall sont perçues comme étant contenues dans le volume du couloir même si elles sont bien plus grandes. Cette perception entraîne un espace volumétriquement illimité, mais géographiquement limité.

Parcours

Je me concentre sur les trois pièces qui contiennent une balise en prenant soin d’éviter de passer devant celles qui n’en contiennent pas, rester d’un côté du couloir me permet de me centrer sur la pièce C et d’identifier les autres pièces par rapport à celle-ci (à droite ou à gauche de la pièce C)
Je remarque un effet imprévu qui me téléporte dans la pièce d’à côté quand je rentre en collision avec le bord du connecteur, pas utilisé, car difficile à prévoir.

![](../images/MEMO/fich1.2.jpg)

### Compte rendu de la seconde session

Parcours

L’effet imprévu peut être maîtrisé si je touche frontalement le cadre du connecteur dans la direction voulue. Trouver le bon angle demande de la concentration c’est pourquoi je commence par faire de courts enchaînements avant de revenir à la méthode normale. À la fin de la session, j’utilise principalement l’effet imprévu pour me déplacer entre les pièces.

Perceptions architecturales

La méthode de déplacement par la téléportation fait gagner un peu de temps, mais génère une lourde fatigue mentale, car l’on ne voit pas l’endroit où l’on est téléporté.
Le couloir n’est pas utilisé, ce qui rend la perception géographique très abstraite, les pièces sont ressenties comme étant isolées dans des volumes indépendants et non connectés aux autres     

![](../images/MEMO/fich1.3.jpg)

### Graphiques des résultats

![](../images/MEMO/donne/hallAB.jpg)
![](../images/MEMO/donne/hallAC.jpg)
![](../images/MEMO/donne/hallBA.jpg)
![](../images/MEMO/donne/hallBC.jpg)
![](../images/MEMO/donne/hallCA.jpg)
![](../images/MEMO/donne/hallCB.jpg)

## Connecteurs en Série

### Représentation axionométrique

![](../images/MEMO/fich2.1.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/-A7GbM2ObdY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Compte rendu de la première session

Perceptions architecturales

L’axe des connecteurs est impossible à placer dans l’espace, il est utilisable quand on connaît son fonctionnement, mais la position géographique et l’orientation des couloirs latéraux sont indéterminables d’un point de vue intérieur.
On peut retenir des raccourcis précis pour des trajets précis.

Parcours

Le personnage ayant une grande inertie, je minimise les tournants au détriment de la distance. Je fais un tracé en forme de boucle qui utilise principalement les connecteurs pour les trajets arrivant à B qui se situe à la fois dans l’axe des salles et celui des connecteurs.

![](../images/MEMO/fich2.2.jpg)

### Compte rendu de la seconde session

Parcours

Avec de l’entrainement, on peut facilement utiliser l’axe des connecteurs sur tous les trajets en évitant les demi-tours si l’on rejoint à chaque fois la prochaine sortie.

Perceptions architecturales

L’axe des connecteurs est toujours abstrait, son tracé sinusoïdal gagnerait à être remplacé par une règle (type droite pour avancer et gauche pour reculer) pour générer moins de fatigue mentale.

![](../images/MEMO/fich2.3.jpg)

### Graphiques des résultats

![](../images/MEMO/donne/serieAB.jpg)
![](../images/MEMO/donne/serieAC.jpg)
![](../images/MEMO/donne/serieBA.jpg)
![](../images/MEMO/donne/serieBC.jpg)
![](../images/MEMO/donne/serieCA.jpg)
![](../images/MEMO/donne/serieCB.jpg)

## Espace Multiscalaire

### Représentation axionométrique

![](../images/MEMO/fich3.1.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/nEZ6Ip0h_rk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Compte rendu de la première session

Perceptions architecturales

Le sens de rotation est facile à intégrer dans l’espace, la circulation circulaire possède une limite quand le personnage est trop grand pour rentrer dans le connecteur suivant (2 connecteurs à partir de B) et quand il est trop petit pour monter sur les balises (8 connecteurs à partir de B).  

Parcours

Étant donné que la balise B demande une hauteur minimale pour être activée, je conserve majoritairement cette échelle et circule au milieu de la salle avec des utilisations ponctuelles des connecteurs pour A-C et C-A.

![](../images/MEMO/fich3.2.jpg)

### Compte rendu de la seconde session

Parcours

Je circule plus souvent en boucle entre A et C grâce aux connecteurs, mais cela demande une « mise à l’échelle » pour rejoindre B ce qui me fait perdre du temps sur A-B et C-B.

Perceptions architecturales

La vitesse du personnage ne varie pas avec l’échelle de celui-ci, chaque échelle propose donc un rapport vitesse/taille différent et donc un confort différent.
Une échelle réduite engendre une impression de vitesse intéressante, mais peu pratique.
Une grande échelle engendre un sentiment de lenteur frustrant et encombrant

![](../images/MEMO/fich3.3.jpg)

### Graphiques des résultats

![](../images/MEMO/donne/scalaireAB.jpg)
![](../images/MEMO/donne/scalaireAC.jpg)
![](../images/MEMO/donne/scalaireBA.jpg)
![](../images/MEMO/donne/scalaireBC.jpg)
![](../images/MEMO/donne/scalaireCA.jpg)
![](../images/MEMO/donne/scalaireCB.jpg)

## Anneaux de Connexion

### Représentation axionométrique

![](../images/MEMO/fich4.1.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/BP5FHKilwyY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Compte rendu de la première session

Perceptions architecturales

Le décor vide est majoritaire à l’écran, cependant la « skybox » (image de fond à ce vide) comporte un soleil qui permet facilement d’identifier le haut et le bas.
Les faces verticales étant identifiables, on s’oriente facilement dans le niveau.
Les connecteurs offrent une vision sur les trois balises ce qui facilite encore l’identification des faces et l’orientation.
L’anneau n’est pas visible depuis les faces adjacentes, ce qui rend difficile de trouver la jonction avec l’anneau.

Parcours

Je n’utilise pas les anneaux de connexion dans leur entièreté, je préfère utiliser un segment qui correspond à une rotation précise.
Le trajet entre B et C est très efficace, mais d’autres chemins existent.  

![](../images/MEMO/fich4.2.jpg)

### Compte rendu de la seconde session

Parcours

Un autre tracé de même dimension est possible entre B et C passant par la face inférieure et couvrant tous les déplacements possibles.

Perceptions architecturales

Le nombre limité de faces permet de s’orienter instinctivement sur le niveau.
La luminosité offre énormément d’information sur la position géographique.

![](../images/MEMO/fich4.3.jpg)

### Graphiques des résultats

![](../images/MEMO/donne/anneauxAB.jpg)
![](../images/MEMO/donne/anneauxAC.jpg)
![](../images/MEMO/donne/anneauxBA.jpg)
![](../images/MEMO/donne/anneauxBC.jpg)
![](../images/MEMO/donne/anneauxCA.jpg)
![](../images/MEMO/donne/anneauxCB.jpg)

## Espace Compressé

### Représentation axionométrique

![](../images/MEMO/fich5.1.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/02RU0eJSeSs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Compte rendu de la première session

Perceptions architecturales

Les salles sont perçues comme étant attenantes les unes aux autres, seules les fenêtres viennent contredire cette perception, elles font plus fonction de lampe ou d’écran qui renvoient vers un espace plus abstrait.
On ne peut visualiser l’espace retranscrit par les fenêtres en même temps que celui perçu à l’intérieur des salles.
Il y a une contradiction intéressante entre la fenêtre qui est prise pour un écran et le connecteur qui est pris pour une ouverture alors qu’il est composé de deux écrans. Dans un monde numérique, y a-t-il une différence de perception entre une fenêtre et un écran ?

Parcours

L’espace étant plutôt réduit, je décris des boucles entre A et C pour ne pas activer la même balise 2 fois d’affilée.
La balise B est en dehors de la boucle et demande un demi-tour ce qui me fait perdre un peu de temps.

![](../images/MEMO/fich5.2.jpg)

### Compte rendu de la seconde session

Parcours

Déplacement le long d’un axe rectiligne en utilisant les murs pour me stopper aux embranchements. Toutes les balises sont facilement accessibles, mais moins rapidement pour AC et CA.

Perceptions architecturales

Action très mécanique, fonctionnent au « par cœur » qui ne permet de percevoir qu’une toute petite zone autour de soi.

![](../images/MEMO/fich5.3.jpg)

### Graphiques des résultats

![](../images/MEMO/donne/compAB.jpg)
![](../images/MEMO/donne/compAC.jpg)
![](../images/MEMO/donne/compBA.jpg)
![](../images/MEMO/donne/compBC.jpg)
![](../images/MEMO/donne/compCA.jpg)
![](../images/MEMO/donne/compCB.jpg)

## Faces Programmatiques

### Représentation axionométrique

![](../images/MEMO/fich6.1.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/64AS3RgBKh4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Compte rendu de la première session

Perceptions architecturales

L’orientation est très facile, car l’on a une vue directe sur toutes les faces qui composent l’espace.
Le haut et le bas du référentiel extérieur sont indéterminables depuis l’intérieur du niveau, aucune gravité ne se présente comme une gravité principale et la direction du soleil à travers la fenêtre donne peu d’information sur l’orientation.
Chaque face a des interactions avec celle adjacente, ces faces sont les « murs » et sont donc accessible sur une certaine hauteur.
Le statut d’une face varie donc entre sol, mur et plafond, et le mobilier présent sur ces faces change de fonction aussi.
Les changements de gravité provoquent des nausées au bout de 17 min d’expérience (aucun précédent de nausée en faisant de la VR, y compris lors de sessions plus longues).

Parcours

Je suis le parcours le plus court entre les balises en passant par les rampes (bloc qui établit une continuité géométrique entre 2 faces grâce à un arc de cercle d’un certain rayon).
Les 3 balises ont un mur en commun. Depuis ce mur la balise B est facilement accessible, mais les balises C et A demandent un saut hasardeux (surface d’atterrissage pas adaptée).

![](../images/MEMO/fich6.2.jpg)

### Compte rendu de la seconde session

Parcours

Je commence par essayer de sauter depuis le mur commun, mais le canapé pose un problème.
Je tente le même procédé en restant sur la face de la balise C, le frigo et la commode me permettent d’avoir accès aux balises A et B tout en restant dans la même gravité. Cette méthode réduit drastiquement le temps utilisé.

Perceptions architecturales

Le fait de rester sur la même face ne provoque plus de nausée, mais l’expérience du lieu se résume alors à de l’escalade dans une pièce anormalement haute et encombrée.

![](../images/MEMO/fich6.3.jpg)

### Graphiques des résultats

![](../images/MEMO/donne/FacesAB.jpg)
![](../images/MEMO/donne/FacesAC.jpg)
![](../images/MEMO/donne/FacesBA.jpg)
![](../images/MEMO/donne/FacesBC.jpg)
![](../images/MEMO/donne/FacesCA.jpg)
![](../images/MEMO/donne/FacesCB.jpg)

## Faces Programmatiques B

### Représentation axionométrique

![](../images/MEMO/fich7.1.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/O_bqceYihG4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Compte rendu de la première session

Perceptions architecturales

Dans cet arrangement toutes les pièces sont disposées le long d’une boucle de circulation. Ce principe est facile à retenir ce qui rend l’orientation efficace bien que moins instinctif que le niveau Faces.
Ce système de circulation demande plus de détours, mais est moins encombrant et laisse la possibilité d’un aménagement bas le long du bloc de circulation.
Les rampes latérales (ex. : entre la face de la balise A et celle de la fenêtre) sont plus progressives ce qui rend la transition plus agréable et instinctive. Avec des rampes d’un rayon plus réduit, la transition est plus brutale (s’approcher rapidement d’un mur n’est pas habituel).

Parcours

Je suis la boucle de circulation pour rallier les balises avec une préférence pour le segment qui traverse la face B, le recours systématique à la boucle de circulation individualise les différentes faces.
Je note une possibilité de saut pour rejoindre C sans l’utiliser.

![](../images/MEMO/fich7.2.jpg)

### Compte rendu de la seconde session

Parcours

La balise B est facilement accessible depuis la face C et la balise C est facilement accessible depuis la salle à manger ce qui me permet de fortement réduire les trajets entre C et B et entre A et C.
Ces 2 méthodes me poussent à utiliser toute la boucle de circulation, mais me font perdre du temps sur certains trajets (ex. : dans le cas de CB > BA, je perds du temps sur BA) ce qui entraîne un temps moyen plus aléatoire.

Perceptions architecturales

Les rampes ont un double statut, étant plus larges elles constituent une face (et donc une pièce) à part entière quand on les arpente, mais elles prennent le statut de meuble quand on se tient à côté.
Les rampes permettent aussi de lire l’espace, elles donnent un cadre à chaque face qui dispose d’une « entrée » unique et d’un espace de circulation perpendiculaire à celle-ci.

![](../images/MEMO/fich7.3.jpg)

### Graphiques des résultats

![](../images/MEMO/donne/FacesBAB.jpg)
![](../images/MEMO/donne/FacesBAC.jpg)
![](../images/MEMO/donne/FacesBBA.jpg)
![](../images/MEMO/donne/FacesBBC.jpg)
![](../images/MEMO/donne/FacesBCA.jpg)
![](../images/MEMO/donne/FacesBCB.jpg)

## Ruban Programmatique

### Représentation axionométrique

![](../images/MEMO/fich8.1.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/PMWtGj9_zfo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Compte rendu de la première session

Perceptions architecturales

Le niveau présente deux circulations entrelacées qui définissent deux entités composées d’un intérieur et d’un extérieur. Les balises sont disposées sur une seule des deux faces du ruban il n’y a donc pas d’interférence avec la seconde entité.
L’espace est cyclique ce qui implique qu’il y a toujours au moins 2 chemins vers un objectif.
L’espace est perçu comme étant deux boucles indépendantes (une extérieure et une intérieure) qui sont connectées par un pont qui prend ici la forme d’un escalier.

Parcours

Je décris une boucle entre C et B avec un détour pour rejoindre A, pour le trajet CA la gravité me permet de sauter de la face C à travers la porte de la boucle intérieure pour rejoindre directement A.

![](../images/MEMO/fich8.2.jpg)

### Compte rendu de la seconde session

Parcours

Je ne marque pas de changement majeur dans ma stratégie à l’exception près que j’enjambe la porte de la seconde entité pour éviter un détour vers B.

Perceptions architecturales

La rampe intérieure dotée d’un rayon élevé ne provoque pas de nausée, et celle extérieure non plus, pourtant elles ont un rayon très réduit (une rampe extérieure demande moins de contraintes ?).
En enjambant la porte de la seconde entité, j’empiète sur celle-ci. Cela pose la question de la relation entre deux occupants géographiquement proches, mais séparés par une gravité différente.

![](../images/MEMO/fich8.3.jpg)

### Graphiques des résultats

![](../images/MEMO/donne/RubanAB.jpg)
![](../images/MEMO/donne/RubanAC.jpg)
![](../images/MEMO/donne/RubanBA.jpg)
![](../images/MEMO/donne/RubanBC.jpg)
![](../images/MEMO/donne/RubanCA.jpg)
![](../images/MEMO/donne/RubanCB.jpg)

## conclusion des résultats

Au cours de ces sessions de test, nous avons pu identifier pour chaque principe les opportunités architecturales, ergonomiques ou programmatiques mais également les risques pour le confort de l’utilisateur. Certains principes sont appréhendés de façon très instinctive alors que d’autres demandent une constante concentration pour être utilisé efficacement.
L’analyse des comptes rendus demande une implication de secteur extérieur à l’architecture pour compléter notre compréhension de ces espaces, en effet de nouvelles limites propres aux logiciels et dispositifs encadrent la création numérique et notre capacité à interpréter ces espaces. De plus, les effets, parfois très violents, induits par les principes d’architecture impossible nous encouragent à anticiper les enjeux à long terme de tels espaces.
