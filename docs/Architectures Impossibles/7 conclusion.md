# 7. Conclusion

Dans un contexte où les espaces numériques prennent une place de plus en plus importante dans la sphère publique, ce mémoire avait pour ambition l’identification des effets architecturaux propres à l’architecture impossible numérique et des outils d’architecture pertinents pour représenter et concevoir ces espaces numériques.
Il a fallu dans un premier temps définir la place des architectures impossibles par rapport aux autres architectures de fiction plus particulièrement par rapport à l’architecture utopique, et la place qu’occupe la réflexion architecturale dans le domaine du jeu vidéo.

Au moyen de courtes sessions de test sur des terrains d’expérimentation numérique, des données et des témoignages ont pu être recueillis afin d’évaluer les effets sur l’utilisateur de sept principes d’architecture impossible dans un contexte proche d’une situation de travail.

Ces sessions d’expérimentation ont permis de mettre en évidence une série de limites conditionnant la conception d’espaces numériques. Ces limites peuvent être classées en trois catégories.
Tout d’abord, les limites logicielles influencées par le choix du programme et son degré de complexité, le choix du programme détermine aussi la compatibilité avec des dispositifs ou les ressources disponibles. Ensuite on peut distinguer les limites matérielles fixant la limite de puissance de calcul et donc l’ampleur ou la complexité d’un niveau. Le matériel conditionne aussi les possibilités d’interaction avec un monde numérique. Enfin, les limites physiologiques influent sur notre capacité à interpréter les signaux émis par un dispositif comme un espace en trois dimensions.

Parallèlement, la construction numérique des niveaux a permis de définir la pertinence d’outils d’architecture et d’esthétique pour le numérique, la représentation est déterminée par une dualité entre espace perçu et espace conçu, la forme schématique a été favorisée pour exprimer les principes d’architecture impossible.
L’axonométrie éclatée a également été utilisée pour son adaptabilité à différentes situations. Au sujet de l’esthétique des rendus, la « Liminal Space Aesthetic » a été privilégiée pour les similitudes de ressenti qu’elle partage avec les rendus numériques. La construction des niveaux a également permis de mettre en évidence les éléments définissant une ergonomie propre au numérique.

Enfin, l’intensité de certains effets numériques architecturaux sur la physiologie humaine, et l’objectif assumé de certaines entreprises de voir des espaces de travail numérique émerger posent la question d’une politique des espaces numériques.

D’une manière similaire à un code d’urbanisme, ce mémoire insiste sur la nécessité d’un contrôle des qualités de vie et des conditions de travail offert par un espace numérique.
Ce mémoire met en évidence la question architecturale centrale des espaces numériques, mais également la pertinence d’une étude transdisciplinaire afin de déterminer précisément tous les enjeux (social, psychologique, technologique) des espaces numériques lors d’exposition de longue durée, pour permettre l’établissement d’un processus de vérification de la qualité d’espace.
